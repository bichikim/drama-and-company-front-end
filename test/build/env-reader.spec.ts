import envReader from '@@/build/env-reader'

describe('env-reader', function test() {
  it('should get APP_XXX env', function test() {
    const env = {
      FOO: 'foo',
      APP_FOO: 'app_foo',
    }
    expect(envReader(env)).to.deep.equal({
      APP_FOO: JSON.stringify('app_foo'),
    })
  })
  it('should get QUASAR_XXX env', function test() {
    const env = {
      FOO: 'foo',
      QUASAR_FOO: 'app_foo',
    }
    expect(envReader(env, 'QUASAR')).to.deep.equal({
      QUASAR_FOO: JSON.stringify('app_foo'),
    })
  })
})
