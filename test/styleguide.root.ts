import Vue from 'vue'
import boot from './src/boot'
import i18n from '@/boot/i18n'

Vue.config.devtools = true

export default (previewComponent) => {
  const bootWait = boot([i18n], Vue)

  return {
    async created(this: any) {
      const {app} = await bootWait
      Object.assign(this.$options, app)
    },
    render(h) {
      return h(previewComponent)
    },
  }
}
