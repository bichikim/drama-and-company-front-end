import {VueConstructor} from 'vue'
import {FunctionModule} from '@/store'
import StoreContext from '@/store/context'
import {BootFileFunction, BootFileParams} from '@/types'
import VueRouter from 'vue-router'
import Vuex, {Store} from 'vuex'

interface ModuleInfo<Store> {
  name: string
  module: FunctionModule<Store, any>
}

const createStoreModule = async <Store>(
  vue: VueConstructor,
  moduleInfo: ModuleInfo<Store>,
  boots: BootFileFunction[] = [],
  ): Promise<any> => {
  const storeContext = StoreContext(vue)
  vue.use(Vuex)

  const context: BootFileParams = {
    app: {},
    Vue: vue,
    router: new VueRouter({}),
    store: new Store({
      modules: {
        [moduleInfo.name]: moduleInfo.module(storeContext.get()),
      },
    }),
  }
  const wait = boots.reduce((wait, boot) => {
    wait.push(boot(context))
    return wait
  }, [] as any[])

  await Promise.all(wait)

  return context
}

export default createStoreModule
