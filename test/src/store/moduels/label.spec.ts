import axiosBoot from '@/boot/axios'
import Labels from '@/store/modules/labels'
import {BootFileParams} from '@/types'
import {createLocalVue} from '@vue/test-utils'
import MockAdapter from 'axios-mock-adapter'
import {pick} from 'lodash'
import createStoreModule from '../create-store-module'

interface MochaContext extends Mocha.Context {
  context: BootFileParams
}

const labels: any[] = [
  {
    '_id': '5afbee91141592fc9850ae38',
    'updatedAt': '2018-05-16T08:53:19.084Z',
    'createdAt': '2018-05-16T08:40:49.193Z',
    'title': 'titleUpdated',
    'memos': [
      {
        '_id': '5afbe6a12c7caff319d454d8',
        'updatedAt': '2018-05-16T08:07:47.688Z',
        'createdAt': '2018-05-16T08:06:57.499Z',
        'title': 'memo1',
        'content': 'memo1',
      },
      {
        '_id': '5afbe6a12c7caff319d454d2',
        'updatedAt': '2018-05-16T08:07:47.688Z',
        'createdAt': '2018-05-16T08:06:57.499Z',
        'title': 'memo1',
        'content': 'memo1',
      },
    ],
  },
  {
    '_id': '5afbf1bd144298fffa301c0e',
    'updatedAt': '2018-05-16T08:54:21.550Z',
    'createdAt': '2018-05-16T08:54:21.550Z',
    'title': 'titleExample',
    'memos': [],
  },
]

describe('store/label', function test() {

  before('set label store module', async function test() {
    this.context = await createStoreModule(
      createLocalVue(),
      {name: 'labels', module: Labels},
      [axiosBoot],
    )
  })


  describe('actions', function test() {
    describe('getLabelList', function test() {
      it('should get label list', async function test() {
        const {store, Vue} = this.context as BootFileParams
        const axios = Vue.storeContext.get().axios()
        const mock = new MockAdapter(axios)

        mock.onGet('/labels').reply((config) => {
          const {params: {populate = false} = {}} = config
          return [200, {
            labels: labels.reduce((labels, label) => {
              const _label = {...label}
              if(populate && Array.isArray(label.memos)) {
                _label.memos = label.memos.map((memo) => (pick(memo, ['_id'])))
              }
              labels.push(_label)
              return labels
            }, []),
          }]

        })

        await store.dispatch('labels/getLabelList')
        expect(store.state.labels.labels).to.deep.equal({
          '5afbee91141592fc9850ae38': {
            'updatedAt': '2018-05-16T08:53:19.084Z',
            'createdAt': '2018-05-16T08:40:49.193Z',
            'title': 'titleUpdated',
            'memos': [
              {'_id': '5afbe6a12c7caff319d454d8'},
              {'_id': '5afbe6a12c7caff319d454d2'},
            ],
          },
          '5afbf1bd144298fffa301c0e': {
            'updatedAt': '2018-05-16T08:54:21.550Z',
            'createdAt': '2018-05-16T08:54:21.550Z',
            'title': 'titleExample',
            'memos': [],
          },
        })
      })
    })
  })
})
