import {FunctionModule, RootState} from '@/store'
import {createRecord} from '@/store/utils'
import {omit} from 'lodash'
import Vue from 'vue'

export interface Label {
  updateAt: string
  createAt: string
  title: string
  memos: string
}

export interface ResponseLabel extends Label{
  _id: string
}

export type LabelRecord = Record<string, Label>

export interface LabelsState {
  labels: LabelRecord | null
  loading: boolean
}

declare module '@/store/types' {
  interface ModuleState {
    labels: LabelsState
  }
}

/**
 * GetLabelList action payload type
 */
export interface GetLabelListPayLoad {
  /**
   * If set false, response labels' memos are just id list
   * @default true
   */
  populate?: boolean
}

const module: FunctionModule<LabelsState, RootState> = (context) => {
  return {
    namespaced: true,
    state: {
      labels: null,
      loading: true,
    },
    getters: {
      memoLabels(state) {
        const {labels} = state
        if(!labels) {
          return {}
        }
        return Object.keys(labels).reduce((result, key) => {
          const label: any = labels[key]
          label.memos.forEach((memo) => {
            if(!result[memo._id]) {
              result[memo._id] = {}
            }
            Object.assign(result[memo._id], {[key]: label.title})
            return result
          })
          return result
        }, {})
      },
      labelNames(state) {
        const {labels} = state
        if(!labels) {
          return []
        }

        return Object.keys(labels).map((key) => {
          return {
            _id: key,
            title: labels[key].title,
          }
        })
      },
    },
    actions: {
      async createLabel({commit}, title: string) {
        const response = await context.axios()({
          method: 'POST',
          url: '/labels',
          data: {
            title,
          },
        })

        commit('updateLabel', response.data)
      },
      async updateLabel({commit}, payload: {title: string, id: string}) {
        const {id, ...others} = payload
        const response = await context.axios()({
          method: 'PUT',
          url: `/labels/${id}`,
          data: others,
        })

        commit('updateLabel', response.data)
      },
      async deleteLabel({commit} , labelId: string) {
        await context.axios()({
          method: 'DELETE',
          url: `/labels/${labelId}`,
        })
        commit('deleteLabel', labelId)
      },
      async getLabelList({commit}, payload: GetLabelListPayLoad = {}) {
        const {populate = true} = payload
        commit('changeLoading', true)
        // Get label list
        const response = await context.axios()({
          method: 'GET',
          url: '/labels',
          params: {
            //  response labels' memos are just id list
            populate,
          },
        })
        commit('changeLoading', false)
        // Save labels
        commit('replaceLabels', response.data)
      },
      async addLabel({commit}, payload: {labelId: string, memoIds: string[]}) {
        const {labelId, memoIds} = payload
        const response = await context.axios()({
          method: 'POST',
          url: `/labels/${labelId}/memos`,
          data: {
            memoIds,
          },
        })

        commit('updateLabel', response.data)
      },
    },
    mutations: {
      replaceLabels(state, labels: ResponseLabel[] = []) {
        state.labels = createRecord(labels)
      },
      updateLabel(state, label: ResponseLabel) {
        if(state.labels) {
          Vue.set(state.labels, label._id, omit(label, '_id'))
        }
      },
      deleteLabel(state, labelId: string) {
        if(state.labels) {
          Vue.delete(state.labels, labelId)
        }
      },
      changeLoading(state, toggle: boolean) {
        state.loading = toggle
      },
    },
  }
}

export default module

