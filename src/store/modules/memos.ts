import {FunctionModule, RootState} from '@/store'
import {createRecord} from '@/store/utils'
import {omit} from 'lodash'
import Vue from 'vue'

export interface Memo {
  content: string
  title: string
  updatedAt: string
  createdAt: string
}

export interface ResponseMemo extends Memo {
  _id: string
}

export type MemoRecord = Record<string, Memo>

export interface MemosState {
  allMemos: MemoRecord | null
  memos: MemoRecord | null
  currentMemoId: string | null
  currentLabelId: string | null
  loading: boolean
}

declare module '@/store/types' {
  interface ModuleState {
    memos: MemosState
  }
}

const module: FunctionModule<MemosState, RootState> = (context) => {
  return {
    namespaced: true,
    state: {
      allMemos: null,
      memos: null,
      loading: true,
      currentMemoId: null,
      currentLabelId: null,
    },
    getters: {
      totalMemos(state): number {
        return state.allMemos ? Object.keys(state.allMemos).length : 0
      },
      currentMemo(state): ResponseMemo | null{
        const {currentMemoId} = state
        if(!state.memos || !currentMemoId) {
          return null
        }

        return {
          ...state.memos[currentMemoId],
          _id: currentMemoId,
        }
      },
    },
    actions: {
      async getAllMemos({commit}) {
        commit('changeLoading', true)
        const response = await context.axios()({
          method: 'GET',
          url: 'memos',
        })
        commit('changeLoading', false)
        commit('replaceAllMemos', response.data)
      },
      async getMemoByLabel({state, commit}, labelId) {
        if(state.currentLabelId === labelId) {
          return
        }
        commit('saveCurrentLabel', labelId)
        commit('changeLoading', true)
        const response = await context.axios()({
          method: 'GET',
          url: `labels/${labelId}`,
        })
        commit('changeLoading', false)
        commit('replaceMemos', response.data.memos)
      },
      async getMemoById({state, commit}, memoId) {
        if(state.currentLabelId === memoId) {
          return
        }
        const response = await context.axios()({
          method: 'GET',
          url: `memos/${memoId}`,
        })

        commit('updateMemo', response.data)
      },
      async updateMemo({commit}, payload: Pick<ResponseMemo, '_id' | 'title' | 'content'>) {
        const {_id, ...others} = payload
        const response = await context.axios()({
          method: 'PUT',
          url: `memos/${_id}`,
          data: others,
        })

        commit('updateMemo', response.data)
      },
      async addLabel({commit}, payload: {memoId: string, labelId: string}) {
        const {labelId, memoId} = payload
        const response = await context.axios()({
          method: 'POST',
          url: `labels/${labelId}/memos`,
          data: {
            memoIds: [memoId],
          },
        })
        commit('labels/updateLabel', response.data, {root: true})
      },
      async deleteLabel({commit}, payload: {memoId: string, labelId: string}) {
        const {labelId, memoId} = payload
        const response = await context.axios()({
          method: 'DELETE',
          url: `labels/${labelId}/memos`,
          data: {
            memoIds: [memoId],
          },
        })

        commit('labels/updateLabel', response.data, {root: true})
      },
      async createEmptyMemo({state, commit, dispatch}, labelId?: string) {
        const response = await context.axios()({
          method: 'POST',
          url: 'memos',
          data: {
            title: 'untitled',
          },
        })

        commit('updateAllMemos', response.data)
        if(state.currentLabelId === labelId) {
          commit('updateMemo', response.data)
        }

        if(labelId) {
          const {_id} = response.data
          await dispatch('addLabel', {memoId: _id, labelId})
        }
      },
      async deleteMemoById({state, commit}, memoId) {

        await context.axios()({
          method: 'DELETE',
          url: `memos/${memoId}`,
        })

        commit('deleteMemo', memoId)

        if(state.currentMemoId === memoId) {
          commit('changeCurrentMemoId', null)
        }
      },
    },
    mutations: {
      replaceAllMemos(state, memos: ResponseMemo[]) {
        state.allMemos = createRecord(memos)
      },
      replaceMemos(state, memos: ResponseMemo[]) {
        state.memos = createRecord(memos)
      },
      updateAllMemos(state, memo: ResponseMemo) {
        if(state.allMemos) {
          Vue.set(state.allMemos, memo._id, omit(memo, '_id'))
        }
      },
      updateMemo(state, memo: ResponseMemo) {
        if(state.memos) {
          Vue.set(state.memos, memo._id, omit(memo, '_id'))
        }
      },
      deleteMemo(state, memoId) {
        if(state.memos) {
          Vue.delete(state.memos, memoId)
        }
        if(state.allMemos) {
          Vue.delete(state.allMemos, memoId)
        }
      },
      saveCurrentLabel(state, labelId: string) {
        state.currentLabelId = labelId
      },
      changeLoading(state, toggle: boolean) {
        state.loading = toggle
      },
      changeCurrentMemoId(state, memoId: string | null) {
        state.currentMemoId = memoId
      },
    },
  }
}

export default module
