import {ContextRecode} from './context'
import {Module} from 'vuex'

export type FunctionModule<S, R> = (context: ContextRecode) => Module<S, R>

export interface ModuleState {
}
