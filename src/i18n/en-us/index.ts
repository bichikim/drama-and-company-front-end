// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  navigation: {
    reservations: 'reservations',
    smootie: 'smootie',
  },
}
