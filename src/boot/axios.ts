import {BootFileFunction} from '@/types'
import Axios, {AxiosInstance} from 'axios'
import Vue from 'vue'

declare module '@/store/context' {
  interface ContextRecode {
    axios: () => AxiosInstance
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    axios?: AxiosInstance
  }
}

interface Options {
  baseURL?: string,
}

const i18n: BootFileFunction = ({app, Vue}, options: Options = {}) => {
  const {baseURL = process.env.QUASAR_BASE_URL} = options

  // Set i18n instance on app
  const axios = Axios.create({
    baseURL,
  })

  app.axios = axios

  Vue.storeContext.set('axios', () => (axios))
}

export default i18n
