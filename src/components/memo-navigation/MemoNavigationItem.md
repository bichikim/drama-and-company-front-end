```
const bind = {
id: 'foo', title: 'foo', description: 'foo foo foo', date: 35346363, loading: false,
active: false,
}
<NoteNavigationItem v-bind="bind" />
<q-separator spaced />
<q-input v-model="bind.title" />
<q-input v-model="bind.description" />
<q-toggle v-model="bind.loading" label="Loading" />
<q-toggle v-model="bind.active" label="Ative" />
```
