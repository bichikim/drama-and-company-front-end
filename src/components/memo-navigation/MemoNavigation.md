```
const items = [
    {id: 'foo', title: 'foo', description: 'foo foo foo', date: 35346363, loading: false},
    {id: 'bar', title: 'bar', description: 'bar bar bar', date: 35346363, loading: false, active
: true},
    {id: 'john', title: 'john', description: 'john john john', date: 35346363}
]
const title = 'foo-title'
<NoteNavigation :items="items" :title="title" />
```
