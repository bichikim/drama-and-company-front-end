```
const bind = {name: 'foo', loading: true, id: 'foo', active: true}
<LabelNavigationItem v-bind="bind" />
<q-separator spaced/>
<q-input v-model="bind.name" label="Name" />
<q-toggle v-model="bind.loading" label="Loading" />
<q-toggle v-model="bind.active" label="Active" />
```

