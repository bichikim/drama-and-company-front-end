import {date} from 'quasar'

export default function formatDate(value, format: string = 'YYYY. MM. DD') {
  return date.formatDate(value, format)
}
