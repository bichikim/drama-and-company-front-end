# Drama And Company Front End Test

## Installation 

(Mac, Ubuntu, Windows)

```
    yarn install
```

## Start service in development mode 

(Mac, Ubuntu, Windows)

```
    yarn dev
```

## Style Guide

(Mac, Ubuntu, Windows)

```
    yarn styleguide
```


## Test

### Test node modules 

(Mac, Ubuntu, Windows)

```
    yarn test:node
```

### Test unit Test 

(Mac, Ubuntu, Windows)

```
    yarn test:unit
```

## UI 팁

- 라벨리스트를 왼쪽으로 밀어서 지울 수 있습니다.
- 제목을 클릭하여 변경 할 수 있습니다.

## 아쉬운 점
back-end 부분을 수정 하고 싶었지만 시간이 부족 하였다.
시간이 부족하여 요구 피쳐만 완성 했다.

### Window 실행을 위한 안내

윈도우에서는
```
PORT=3000 npm start
```
가 안되는데

(cross-env)[https://www.npmjs.com/package/cross-env] 로 해결 가능 하다.

### memo 가 가지고 있는 라벨들을 알 수가 없다

### 미리 쓸 데이터를 넣어 줄 수 있다.

[json-server](https://www.npmjs.com/package/json-server)
를 쓰면 미리 쓸 데이터를 넣어 줄 수 있었을 것입니다.

### 여러 메모릴 지울 수 있는 end-point 가 필요하다.

### label 추가
메모에 대하여 라벨을 한번에 많이 붙이기 위해\
POST\
/memos/:id/labels\
가 있었 으면  더 좋을 것같다.

## 사용한 프레임 워크

Vue.js, Quasar, vue-styleguide, karma, mocha, 기타 package.json>dependencies 또는
 package.json>devDependencies 참조
 

## 플로우

컴포넌트 > 컴포넌트 > 레이아웃 또는 페이지 > 스토어 > API request > 데이터 저장

테이터 저장 > 레이하웃 또는 페이지 > 컴포넌트 > 컴포넌트

## 궁금한 사항

### 쓰기 쉬운 것을 두고 mongodb 를 express 랑 쓴 걸까?

[json-server](https://www.npmjs.com/package/json-server) 를 사용하면 좋을 것 같다.

mongodb SaaS 서비스가 있어서 요즘 잘 mongodb 를 설치 안하는 데 이 테스트 때문에 설치하게 되었다.

