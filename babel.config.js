module.exports = {
  presets: [
    [
      '@quasar/babel-preset-app', {
      // for tree shacking
      modules: false,
    }],
    /**
     * babel ❤ ts ❤ vue
     * @link https://www.npmjs.com/package/babel-preset-typescript-vue
     */
    [
      'typescript-vue',
      {
        isTSX: true,
        allExtensions: true,
      },
    ],
  ],
  plugins: [
    // for typescript decorators
    ['@babel/plugin-proposal-decorators', {legacy: true}],
    // for typescript class properties
    ['@babel/proposal-class-properties', {loose: true}],
    // for typescript optional chaining 'foo?.bar'
    '@babel/plugin-proposal-optional-chaining',
  ],
  env: {
    test: {
      // for coverage
      plugins: ['istanbul'],
    },
  },
}
